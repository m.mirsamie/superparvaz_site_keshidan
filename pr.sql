DROP PROCEDURE IF EXISTS PR_PARVAZ_DET;
delimiter //
CREATE PROCEDURE PR_PARVAZ_DET()
BEGIN
	DECLARE pw_date DATE DEFAULT '0000-00-00';
	DECLARE pw_saat TIME DEFAULT '00:00:00';
	DECLARE pw_tedad INT;
	DECLARE pw_ghimat INT;
	DECLARE pw_parvaz INT;
	DECLARE pw_site_id INT;
	DECLARE pw_add_ghimat INT;
	DECLARE pw_ghimat_final INT;
	DECLARE pt_ghimat INT;
	DECLARE pt_zarfiat INT;
	DECLARE pt_en INT;
	DECLARE update_id INT;
	DECLARE insert_id INT;
	DECLARE pt_site_id INT;
	DECLARE empty_data INT DEFAULT 0;
	DECLARE found_rows_count INT DEFAULT 0;
	DECLARE parvaz_exists INT DEFAULT 0;
	DECLARE i_count INT DEFAULT 0;
	DECLARE parvaz_web_cursor CURSOR FOR SELECT `date`, `saat`, `tedad`, `ghimat`, `parvaz`, `site_id`,`add_ghimat` FROM parvaz_web;
	DECLARE CONTINUE HANDLER 
	        FOR NOT FOUND SET empty_data = 1;
	OPEN parvaz_web_cursor;
	SET found_rows_count = FOUND_ROWS();
	IF found_rows_count>0 THEN
		parvaz_web_loop : LOOP
			SET parvaz_exists = 0;
			SET update_id = 0;
			IF i_count < found_rows_count THEN
				FETCH parvaz_web_cursor INTO pw_date,pw_saat,pw_tedad,pw_ghimat,pw_parvaz,pw_site_id,pw_add_ghimat;
				SET pw_ghimat_final = pw_ghimat + pw_add_ghimat;
				SELECT `id` INTO update_id FROM parvaz_det WHERE parvaz_id = pw_parvaz and tarikh = pw_date and en <> 0;
				SELECT `sites_id` INTO pt_site_id FROM parvaz_det_sites  WHERE parvaz_det_id = update_id;
				SET parvaz_exists = FOUND_ROWS();
				IF parvaz_exists = 0 THEN
					INSERT INTO parvaz_det (`parvaz_id`, `tarikh`, `saat`, `zarfiat`, `ghimat`, `typ` , `en`) VALUES (pw_parvaz,pw_date,pw_saat,pw_tedad,pw_ghimat_final,0,1);
					SET insert_id = LAST_INSERT_ID();
					INSERT INTO parvaz_det_sites (`sites_id`,`parvaz_det_id`) VALUES (pw_site_id,insert_id);
				ELSE
					UPDATE parvaz_det SET saat = pw_saat,zarfiat = pw_tedad,ghimat = pw_ghimat_final,en = IF(en=3,1,en) WHERE parvaz_id = pw_parvaz AND tarikh = pw_date AND (en = 3 OR (pw_ghimat_final <> ghimat AND pt_site_id = pw_site_id) OR (pw_ghimat_final < ghimat AND pt_site_id <> pw_site_id) OR (pw_ghimat_final=ghimat AND zarfiat<>pw_tedad));
					IF ROW_COUNT() > 0 THEN
						UPDATE parvaz_det_sites SET sites_id=pw_site_id WHERE parvaz_det_id = update_id;
					END IF;
				END IF;
				SET i_count = i_count + 1;
			ELSE
				LEAVE parvaz_web_loop;
			END IF;
			
		END LOOP parvaz_web_loop;
	END IF;
	CLOSE parvaz_web_cursor;
END//
delimiter ;
